package pl.sda.collections;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class SetyZad3 {
    public static void main(String[] args) {


        Set<ParaLiczb> secik3 = new HashSet<>(Arrays.asList(new ParaLiczb(1, 2), new ParaLiczb(2, 1)
                , new ParaLiczb(1, 1), new ParaLiczb(1, 2)));

        System.out.println(secik3);
    }
}
