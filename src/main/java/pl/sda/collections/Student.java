package pl.sda.collections;

import java.util.ArrayList;
import java.util.List;

public class Student  {
    private long numer_indeksu;
    private String imie;
    private String nazwisko;
    private Plec plec;

    public void setNumer_indeksu(long numer_indeksu) {
        this.numer_indeksu = numer_indeksu;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public void setPlec(Plec plec) {
        this.plec = plec;
    }

    public long getNumer_indeksu() {

        return numer_indeksu;
    }

    public String getImie() {
        return imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public Plec getPlec() {
        return plec;
    }

    public Student(long numer_indeksu, String imie, String nazwisko, Plec plec) {

        this.numer_indeksu = numer_indeksu;
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.plec = plec;
    }

}
