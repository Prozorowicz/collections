package pl.sda.collections;

import java.util.Objects;

public class ParaLiczb {
    private int a;
    private int b;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ParaLiczb paraLiczb = (ParaLiczb) o;
        return getA() == paraLiczb.getA() &&
                getB() == paraLiczb.getB();
    }

    @Override
    public String toString() {
        return "ParaLiczb{" +
                "a=" + a +
                ", b=" + b +
                '}';
    }

    @Override
    public int hashCode() {

        return Objects.hash(getA(), getB());
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    public ParaLiczb(int a, int b) {

        this.a = a;
        this.b = b;
    }
}
