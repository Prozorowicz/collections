package pl.sda.collections;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class SetyZad2 {
    public static void main(String[] args) {
        String text = "yyo.";
        System.out.println(containDuplicates(text));
    }

    static boolean containDuplicates(String text) {
        String[] literki = text.split("");
        Set<String> setLiterek = new HashSet<>();
        setLiterek.addAll(Arrays.asList(literki));
        if (literki.length == setLiterek.size()) {
            return false;
        } else {
            return true;
        }
    }
}
