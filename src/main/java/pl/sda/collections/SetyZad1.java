package pl.sda.collections;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class SetyZad1 {
    public static void main(String[] args) {
        Set<Integer> secik = new TreeSet<>(Arrays.asList(10, 12, 10, 3, 4, 12, 12, 300, 12, 40, 55));
        System.out.println("rozmiar "+secik.size());
        for (Integer integer : secik) {
            System.out.println(integer);
        }
        secik.removeAll(Arrays.asList(10, 12));

        System.out.println("rozmiar "+secik.size());
        for (Integer integer : secik) {
            System.out.println(integer);
        }

    }
}
