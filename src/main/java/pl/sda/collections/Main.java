package pl.sda.collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {


    public static void main(String[] args) {
        List<Student> lista = new ArrayList<>(Arrays.asList(new Student(22, "jarek",
                "jarkowicz", Plec.MEZCZYZNA), new Student(44, "monika", "muszynska",
                Plec.KOBIETA), new Student(56, "kuba", "kowalski", Plec.MEZCZYZNA)));

        System.out.println(lista);
        for (Student student : lista){
            System.out.println(student);
        }
        for (Student student : lista){
            if (student.getPlec()==Plec.KOBIETA){
                System.out.println("Jestem kobieta: "+student);
            }
        }
        for (int i = 0;i<lista.size();i++){
            if (lista.get(i).getPlec()==Plec.MEZCZYZNA){
                System.out.println("jestem mezczyzna: "+lista.get(i));
            }
        }
        for (Student studencik: lista){
            System.out.println("indeks : "+studencik.getNumer_indeksu());
        }
    }

}
