package pl.sda.collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Zad2 {
    public static void main(String[] args) {
        List<Integer> lista2 = new ArrayList<>();
        Random random = new Random();
        int suma = 0;
        for (int i = 0;i<10;i++){
            lista2.add(random.nextInt(1000));
            suma += lista2.get(i);
        }
        List<Integer> listaKopia = new ArrayList<>(lista2);
        System.out.println(suma);
        int srednia = suma/10;
        System.out.println(srednia);
        Collections.sort(lista2);
        int mediana = (lista2.get(4)+lista2.get(5))/2;
        System.out.println(mediana);
        int najwieksza=0;
        int indeksNajwiekszej=0;
        for (int i = 0; i<10;i++){
            if (listaKopia.get(i)>=najwieksza) {
                najwieksza = listaKopia.get(i);
                indeksNajwiekszej = i;
            }
        }
        System.out.println(indeksNajwiekszej);
        System.out.println(najwieksza);
        int najmniejsza=0;
        int indeksNajmniejszej=0;
        for (int i = 0; i<10;i++){
            if (listaKopia.get(i)<=najmniejsza) {
                najmniejsza = listaKopia.get(i);
                indeksNajmniejszej = i;
            }
        }
        System.out.println(indeksNajmniejszej);
        System.out.println(najmniejsza);
        System.out.println(listaKopia.indexOf(najmniejsza));
        System.out.println(listaKopia.indexOf(najwieksza));
        System.out.println(lista2.indexOf(najmniejsza));
        System.out.println(lista2.indexOf(najwieksza));

    }
}
